import axios from "axios";
// 192.168.168.139测试地址
// 47.107.244.166:8086，必要时改回来
// 172.18.43.140 远程服务器地址
// 192.168.168.70:80 本地测试IP
const develop = 'http://192.168.168.70:8162/';
const online = 'http://47.107.244.166:8086/';
const online_one = 'http://14.23.112.102:8086/';
// 切换开发和生产环境 develop/online
const ip = online_one;
export default {
    form: {
        ticketOrTranstmoneytocompany: "",
        listno: "",
        repair: "",
        repairimmediately: "",
        docimasy: "",
        docimasyimmediately: "",
        repairpromisecardno: "",
        purchase: "",
        customerName: "",
        address: "",
        connectionman: "",
        stampno: "",
        bugdescribe: "",
        tel1: "",
        tel2: "",
        defaultdocimasyunit: "",
        instrumentName: "",
        model: "",
        instrumentNo: "",
        cont: "",
        instrumentsendman: "",
        attachment1: "",
        attachment2: "",
        attachment3: "",
        attachment4: "",
        attachment5: "",
        attachment6: "",
        attachment1cont: "",
        attachment2cont: "",
        attachment3cont: "",
        attachment4cont: "",
        attachment5cont: "",
        attachment6cont: "",
        otherattachment: "",
        rechandlerman: "",
        recdate: "",
        takeman: "",
        takepassman: "",
        deliverydate: "",
        moneyrecman: "",
        moneyrecdate: "",
        belong: "",
        businessbelong: "",
        property: "",
        // classfy: "",
        repairoptions: "",
        pricemakedate: "",
        hassendpricewaitask: "",
        agreedate: "",
        senddata: "",
        finisheddate: "",
        repairman: "",
        sendtocompanydate: "",
        companyrepairlistno: "",
        returnfromcompanydate: "",
        price: "",
        recedman: "",
        froofman: "",
        docimasyhasrecedmoney: "",
        amountreceived: "",
        verifying: "",
        price2: "",
        check: "",
        cashaccount: "",
        moneytranstedtodocimasyunit: "",
        docimasycoast: "",
        // seruverfee: "",
        purchasecost: "",
        repairtotalcost: "",
        repairoption1: "",
        repairoption1coast: "",
        repairoption4: "",
        repairoption4coast: "",
        repairoption2: "",
        repairoption2coast: "",
        repairoption5: "",
        repairoption5coast: "",
        repairoption3: "",
        repairoption3coast: "",
        outsidesendfactoryname: "",
        outsidesenddate: "",
        returndate: "",
        repairoptionother: "",
        remark: ""
    },
    formscanner2: {
        ticketOrTranstmoneytocompany: false,
        listno: "",
        repair: false,
        repairimmediately: false,
        docimasy: false,
        docimasyimmediately: false,
        repairpromisecardno: "0",
        purchase: "",
        customerName: "",
        address: "0",
        connectionman: "",
        stampno: "0",
        bugdescribe: "",
        tel1: "",
        tel2: "",
        defaultdocimasyunit: "",
        instrumentName: "",
        model: "",
        instrumentNo: "",
        cont: "1",
        instrumentsendman: "0",
        attachment1: "0",
        attachment2: "0",
        attachment3: "0",
        attachment4: "0",
        attachment5: "0",
        attachment6: "0",
        attachment1cont: "0",
        attachment2cont: "0",
        attachment3cont: "0",
        attachment4cont: "0",
        attachment5cont: "0",
        attachment6cont: "0",
        otherattachment: "0",
        rechandlerman: "0",
        recdate: "",
        takeman: "0",
        takepassman: "0",
        deliverydate: "",
        moneyrecman: "0",
        moneyrecdate: "",
        belong: "",
        businessbelong: "0",
        property: "",
        // classfy: "",
        repairoptions: "0",
        pricemakedate: "",
        hassendpricewaitask: "",
        agreedate: "",
        senddata: "",
        finisheddate: "",
        repairman: "0",
        sendtocompanydate: "",
        companyrepairlistno: "",
        returnfromcompanydate: "",
        price: "0.0000",
        recedman: "0.0000",
        froofman: "0",
        docimasyhasrecedmoney: false,
        amountreceived: "",
        verifying: "",
        price2: "0.0000",
        check: "",
        cashaccount: false,
        moneytranstedtodocimasyunit: false,
        docimasycoast: "0",
        // seruverfee: "",
        purchasecost: "0.0000",
        repairtotalcost: "0.0000",
        repairoption1: "0",
        repairoption1coast: "0.0000",
        repairoption4: "0",
        repairoption4coast: "0.0000",
        repairoption2: "0",
        repairoption2coast: "0.0000",
        repairoption5: "0",
        repairoption5coast: "0.0000",
        repairoption3: "0",
        repairoption3coast: "0.0000",
        outsidesendfactoryname: "0",
        outsidesenddate: "",
        returndate: "",
        repairoptionother: "0",
        remark: "0"
    },
    formtemplateHeader: [
        { label: "单号", prop: "listno" },
        { label: "分项", prop: "options" },
        { label: "属性", prop: "classfy" },
        { label: "属地", prop: "belong" },
        { label: "客户名称", prop: "customerName" },
        { label: "型号", prop: "model" },
        { label: "数量", prop: "cont" },
        { label: "维修金额", prop: "price" },
        { label: "成本", prop: "purchasecost" },
        { label: "业务费", prop: "businessmoney" },
        { label: "扣罚金额", prop: "pulishedmoney" },
        { label: "维修提成额", prop: "percentmoney" },
        { label: "完成日期", prop: "finisheddate" }
    ],
    urllist: {
        masterpage: ip + "businessrecording.ashx?type=",
        adduser: ip + "Authority.ashx?type=",
        fileDownload: ip + "calculate.ashx?type=",
        downloadBar: ip + "InteractiveStateInfo.ashx?type=",
        keepFix: ip + "guaranteecard.ashx?type="
    },
    gethttpdata(url, item) {
        axios.get(url).then(response => {
            console.log("外接函数成功，获取数据：", response);
            sessionStorage.setItem(item, JSON.stringify(response));
        }).catch(error => {
            console.log(error)
        })
    },
    //文件下载，默认提供xlsx文件格式   api:下载接口   fileName:文件名
    downLoad(api, fileName) {
        axios({
            method: 'get',
            url: api,
            responseType: 'blob',
            contentType: "application/x-www-form-urlencoded"
        }).then(res => {
            let data = res.data;
            // Blob数据生成下载url
            let href = window.URL.createObjectURL(new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' }));
            // 创建下载元素
            let link = document.createElement('a');
            link.style.display = 'none';
            link.href = href;
            link.setAttribute('download', fileName + '.xlsx');
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link); // 移除下载元素
            window.URL.revokeObjectURL(href); //释放掉blob对象 
        }).catch((error) => {
            console.log(error);
        })
    },
    // 将请求参数转换成FROMdata形式
    transformRequest(data) {
        let ret = "";
        for (let i in data) {
            ret += encodeURIComponent(i) + "=" + encodeURIComponent(data[i]) + "&";
        }
        return ret;
    },
    // 转换时间格式
    pushtime(s) {
        return s < 10 ? "0" + s : s;
    },
    getdatetime(time) {
        let dtime = new Date(time);
        let resDate = dtime.getFullYear() + '/' + this.pushtime((dtime.getMonth() + 1) + '/' + this.pushtime(dtime.getDate()));
        return resDate;
    },
    //获取当前时间
    getnowtime() {
        let myDate = new Date();
        let year = myDate.getFullYear();
        let month = myDate.getMonth() + 1;
        let date = myDate.getDate();
        let fulldate = year + "-" + month + "-" + date;
        return fulldate
    },
    gridtime(time) {
        let dtime = new Date(time);
        let resDate =
            dtime.getFullYear() +
            "/" +
            this.pushtime(dtime.getMonth() + 1) +
            "/" +
            this.pushtime(dtime.getDate());
        return resDate;
    },
    savepointscanner(form) {
        form = this.formscanner2;
        return form
    },
    saveform(form) {
        for (let i in this.form) {
            for (let j in form) {
                if (i == j) {
                    this.form[i] = form[j];
                    form[i] = form[i] === "否" ? false : form[i] == "是" ? true : form[i];
                }
            }
        }
        return this.form
    },
    getdata(form, thatform) { //赋值函数
        for (let i in form) {
            for (let j in thatform) {
                if (i == j) {
                    form[i] = thatform[j];
                    form[i] = (this.selecFullChar(form[i],"2001/") || 
                    this.selecFullChar(form[i],"1899/") ||
                    form[i] === "0" || 
                    this.selecFullChar(form[i],"0.0000") || 
                    form[i] === "111") ? "" : form[i];
                    // form[i] = (form[i] === "2001/1/1 0:00:00" || form[i] === "2001/01/01 0:00:00" || form[i] === "2001/01/01" || form[i] === "0" || form[i] === "0.0000" || form[i] === "111") ? "" : form[i];
                    form[i] = form[i] === "否" ? false : form[i] == "是" ? true : form[i];
                }
            }
        }
    },
    selecFullChar(stringLL , selectChar){//检测前者是否含有后者
        if(stringLL.indexOf(selectChar) != -1){return true}
    },
    selectTime(select, format) { //根据传进来的字段判断返回的时间，还有时间规划格式
        let timestring = "";
        let mydata = new Date();
        let nowyear = mydata.getFullYear();
        let nowmonth = mydata.getMonth() + 1;
        let nowdate = mydata.getDate();
        let nowtime = nowyear + format + nowmonth + format + nowdate;
        timestring = select == "year" ? nowyear : select == "month" ? nowmonth : select == "date" ? nowdate : nowtime;
        return timestring
    },
    formatDate(numb, format) { //修正excel转化的时间格式
        let time = new Date((numb - 1) * 24 * 3600000 + 1);
        time.setFullYear(time.getFullYear() - 70);
        let year = time.getFullYear() + '';
        let month = time.getMonth() + 1 + '';
        let date = time.getDate() - 1 + '';
        if (format && format.length === 1) {
            return year + format + month + format + date
        }
        return year + (month < 10 ? '0' + month : month) + (date < 10 ? '0' + date : date)
    },
    timeformtransform(time, select) { //1：毫秒数转化 ， 2：秒数转化
        let dtime = select === 1 ? new Date((time / 1000) * 1000) : dtime = new Date(time * 1000)
        // (d.getHours()) + ":" +(d.getMinutes()) + ":" +(d.getSeconds());按需加上时分秒
        let ddate = (dtime.getFullYear()) + "-" + (dtime.getMonth() + 1) + "-" + (dtime.getDate())
        return ddate
    },
    timetexure(date) { //检测时间格式
        if (isNaN(date) && !isNaN(Date.parse(date))) {
            return true
        } else {
            return false
        }
    },
    //message决定是否使用提示信息
    axioscomplate(urlstage, method, date) {
        axios({
            url: urlstage,
            method: method,
            data: date,
            headers: { "Content-Type": "application/x-www-form-urlencoded" }
        })
            .then(response => {
                console.log(response);
            })
            .catch(err => {
                if (err && err.response) {
                    switch (err.response.status) {
                        case 400:
                            err.message = '请求错误'
                            break

                        case 401:
                            err.message = '未授权，请登录'
                            break

                        case 403:
                            err.message = '拒绝访问'
                            break

                        case 404:
                            err.message = `请求地址出错: ${err.response.config.url}`
                            break

                        case 408:
                            err.message = '请求超时'
                            break

                        case 500:
                            err.message = '服务器内部错误'
                            break

                        case 501:
                            err.message = '服务未实现'
                            break

                        case 502:
                            err.message = '网关错误'
                            break

                        case 503:
                            err.message = '服务不可用'
                            break

                        case 504:
                            err.message = '网关超时'
                            break

                        case 505:
                            err.message = 'HTTP版本不受支持'
                            break

                        default:
                    }
                }
            })
    }
}