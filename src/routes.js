import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
import Table from './views/nav1/Table.vue'
import Deduction from './views/nav1/Deduction/Deduction.vue'
import keepFix from './views/nav1/keepFix/keepFix.vue'
import Form from './views/nav1/Form.vue'
import Page4 from './views/nav2/Page4.vue'
import RepairMan from "./views/nav2/personnel/repairman.vue";
import RepairManager from "./views/nav2/personnel/repairManager.vue";
import Repairrecordermanager from "./views/nav2/personnel/repairrecordermanager.vue";
import Docimasymanager from "./views/nav2/personnel/docimasymanager.vue";
import Accountant from "./views/nav2/personnel/accountant.vue";
import Selsesman from "./views/nav2/personnel/selsesman.vue";
import Page5 from './views/nav2/Page5.vue'
import Page6 from './views/nav3/Page6.vue'
import echarts from './views/charts/echarts.vue'

let routes = [{
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true,
        visible: true
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '维修管理',
        iconCls: 'el-icon-message', //图标样式class
        visible: true,
        children: [
            { path: '/main', component: Main, name: '主页', hidden: true, visible: true },
            { path: '/table', component: Table, name: '维修单', visible: true },
            { path: '/Deduction', component: Deduction, name: '提成计算', visible: false },
            { path: '/keepFix', component: keepFix, name: '保修卡', visible: true },
            { path: '/form', component: Form, name: 'Form', hidden: true, visible: true },
            // { path: '/user', component: user, name: '列表' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '人员管理',
        iconCls: 'fa fa-id-card-o',
        visible: isShow("personnel"),
        children: [
            // { path: '/page4', component: Page4, name: '维修员' },
            { path: '/repairman', component: RepairMan, name: '维修员', visible: isShow("repairman") },
            { path: '/repairManager', component: RepairManager, name: '维修管理员', visible: isShow("repairManager") },
            { path: '/repairrecordermanager', component: Repairrecordermanager, name: '维修登记管理员', visible: isShow("repairrecordermanager") },
            { path: '/docimasymanager', component: Docimasymanager, name: '检定管理员', visible: isShow("docimasymanager") },
            { path: '/accountant', component: Accountant, name: '财务', visible: isShow("accountant") },
            { path: '/selsesman', component: Selsesman, name: '营销员', visible: isShow("selsesman") },
            { path: '/page5', component: Page5, name: '页面5', hidden: true, visible: true }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-address-card',
        visible: true,
        leaf: true, //只有一个节点
        hidden: true,
        children: [
            { path: '/page6', component: Page6, name: '导航三', visible: true }
        ]
    },
    {
        path: '/',
        component: Home,
        name: 'Charts',
        visible: true,
        iconCls: 'fa fa-bar-chart',
        hidden: true,
        children: [
            { path: '/echarts', component: echarts, name: 'echarts', visible: true }
        ]
    },
    {
        path: '*',
        hidden: true,
        visible: true,
        redirect: { path: '/404' }
    }
];

function chosearray() {
    let userType = sessionStorage.getItem("userType");
    // let userType = JSON.parse(sessionStorage.getItem("userData")).role;
    let array1 = { path: '/page4', component: Page4, name: '维修员' };
    let array2 = { path: '/repairman', component: RepairMan, name: '维修员', visible: isShow("repairman") };
    let array3 = { path: '/repairManager', component: RepairManager, name: '维修管理员', visible: isShow("repairManager") };
    let array4 = { path: '/docimasymanager', component: Docimasymanager, name: '检定管理员', visible: isShow("docimasymanager") };
    let array5 = { path: '/accountant', component: Accountant, name: '财务', visible: isShow("accountant") };
    let array6 = { path: '/selsesman', component: Selsesman, name: '营销员', visible: isShow("selsesman") };
    let array = [array1]
    return array
        // switch (userType) {
        //     case "_supermanager":
        //         let array = new Array;
        //         array.push(array1)
        //         console.log(array, "组装数组")
        //         return array
        // }
}

function isShow(menu) {
    let userType = sessionStorage.getItem("userType");
    switch (userType) {
        case "_accountant":
            // 全部
            // 财务
            return false;
            break;
        case "_docimasymanager":
            // 检定、检定加急
            // 检定管理员
            return false;
            break;
        case "_repairman":
            // 全部
            // 维修员
            return false;
            break;
        case "_repairmanager":
            // 维修、维修加急
            // 维修管理员
            if (menu == "personnel") {
                // console.log("personnel", true);
                return true;
            } else if (menu == "repairman") {
                // console.log("repairman", true);
                return true;
            } else {
                // console.log("other", false);
                return false;
            }
        case "_repairrecordermanager":
            // 全部
            // 维修监督管理员
            if (menu == "repairrecordermanager") {
                return true
            } else {
                return false;
            }
            break;
        case "_supermanager":
            // 全部
            // 超级管理员
            // console.log("超级管理员");
            return true
        default:
            console.log("路由出错！");
            break;
    }
}

export default routes;