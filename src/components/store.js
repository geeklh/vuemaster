/**
 * 本地存储操作工具,local为localStorage,session为SessionStorage
 */
let Store = {
    local: {
        get: function (key) {
            // if (window.Storage && window.localStorage && window.localStorage instanceof Storage) {
            //     console.log("浏览器不支持本地存储");
            // }
            var value = localStorage.getItem(key);
            if (value) {
                try {
                    var value_json = JSON.parse(value);
                    if (typeof value_json === 'object') {
                        return value_json;
                    } else if (typeof value_json === 'number') {
                        return value_json;
                    }
                } catch (e) {
                    return value;
                }
            } else {
                return false;
            }
        },
        set: function (key, value) {
            if (value) {
                try {
                    var valueData = value;
                    var keyData = key;
                    if (typeof valueData === "object") {
                        valueData = JSON.stringify(valueData);
                    } else if (typeof keyData !== "string") {
                        keyData = keyData.toString();
                    }
                    localStorage.setItem(keyData, valueData);
                } catch (error) {
                    console.log("出现错误:", error);
                    return false;
                }
            }
        },
        remove: function (key) {
            localStorage.removeItem(key);
        },
        clear: function () {
            localStorage.clear();
        },
        setList: function (data) {
            for (var i in data) {
                localStorage.setItem(i, data[i]);
            }
        },
        removeList: function (list) {
            for (var i = 0, len = list.length; i < len; i++) {
                localStorage.removeItem(list[i]);
            }
        }
    },
    session: {
        get: function (key) {
            // if (window.Storage && window.sessionStorage && window.sessionStorage instanceof Storage) {
            //     console.log("浏览器不支持本地存储");
            // }
            var value = sessionStorage.getItem(key);
            if (value) {
                try {
                    var value_json = JSON.parse(value);
                    if (typeof value_json === 'object') {
                        return value_json;
                    } else if (typeof value_json === 'number') {
                        return value_json;
                    }
                } catch (e) {
                    return value;
                }
            } else {
                return false;
            }
        },
        set: function (key, value) {
            if (value) {
                try {
                    var valueData = value;
                    var keyData = key;
                    if (typeof valueData === "object") {
                        valueData = JSON.stringify(valueData);
                    } else if (typeof keyData !== "string") {
                        keyData = keyData.toString();
                    }
                    sessionStorage.setItem(keyData, valueData);
                } catch (error) {
                    console.log("出现错误:", error);
                    return false;
                }
            }
        },
        remove: function (key) {
            var keyData = key;
            if (typeof keyData !== "string") {
                keyData = keyData.toString();
            }
            sessionStorage.removeItem(keyData);
        },
        clear: function () {
            sessionStorage.clear();
        },
        setList: function (data) {
            try {
                for (var i in data) {
                    sessionStorage.setItem(i, data[i]);
                }
            } catch (error) {
                console.log("出现错误:", error);
                return false;
            }
        },
        removeList: function (list) {
            for (var i = 0, len = list.length; i < len; i++) {
                sessionStorage.removeItem(list[i]);
            }
        }
    }
}
export default Store;