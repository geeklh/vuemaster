**demo**: [https://taylorchen709.github.io/vue-admin/](https://taylorchen709.github.io/vue-admin/)

# To start

This is a project template for [vue-cli](https://github.com/vuejs/vue-cli)

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8081
npm run dev

# build for production with minification
npm run build

```
   
# Folder structure
* build - webpack config files
* config - webpack config files
* dist - build
* src -your app
    * api
    * assets
    * common
    * components - your vue components
    * mock
    * styles
    * views - your pages
    * vuex
    * App.vue
    * main.js - main file
    * routes.js
* static - static assets

# Theme
You can change theme by 
1. Generate theme packages by [https://elementui.github.io/theme-preview/#/](https://elementui.github.io/theme-preview/#/)
2. Put theme packages in src/assets/theme/
3. Edit src/main.js 
``` bash
   import 'element-ui/lib/theme-default/index.css'
   to
   import './assets/theme/your-theme/index.css'
```
4. Edit src/styles/vars.scss

![theme-blue](https://raw.githubusercontent.com/taylorchen709/markdown-images/master/vueadmin/rec-demo.gif)
![theme-green](https://raw.githubusercontent.com/taylorchen709/markdown-images/master/vueadmin/theme-green.png)

# Browser support

Modern browsers and IE 10+.

# License
[MIT](http://opensource.org/licenses/MIT)

#设计模式
工厂模式+策略模式 = 混合模式开发

#接口管理
登录访问的URL： http://192.168.168.139/Authority.ashx?type=login;

进入桌面URL：http://192.168.168.139/businessrecording.ashx?type=dataquery&city=加上当前账号隶属的城市名称

新增单URL:http://192.168.168.139/businessrecording.ashx?type=add&city=加上当前在界面上选中的城市名称 （含有一个逻辑是检测名称是不是广分，是的话就不要加上城市名称，现在的判断条件是，不管是不是广分都要把城市名称加上）

判断SW是1的就是更新，2是新增

下面的·URL要看他的父级组件有没有开启personbelong（1/2），还有当前的城市是不是在广分
更新URL：http://192.168.168.139/businessrecording.ashx?type=  adminupdate/update  &city=当前选中的城市名称

新增单，如果是没有把时间填上的话，会默认地加上一个2001年的时间

查询URL：http://192.168.168.139/businessrecording.ashx?type=dataquery&belong=所属分公司&city=当前选中的城市名称

单击刷新URL：重新获取列表数据

删除URL：http://192.168.168.139/businessrecording.ashx?type=delete&city=当前选中的城市名称；
代码中有批量删除的方法，不删

#权限管理

_accountant 财务：列表上能看到的只有查看按钮，点击进去之后只能对内容查看，不能对内容进行修改


#shuoming book

在登录界面转到列表页面的过程会有一次刷新动作

事件处理都是 1:开启 2：关闭
所有的处理处理事件都放在了table.vue的switch上，对权限进行控制
perosoncardll 控制表格数据是否可以编辑
buttonshow 控制编辑和删除是否可见
buttonshowcheck 控制查看按钮是否可见
addcity 控制post请求上是否加上city
personbelong 控制在保存的时候是否加上admin
personlistvalue 默认获取列表
proshow 成本/厂家收费

查询条件是全部是时候，不传finished参数；
查询和一开始进来访问的接口应该是用一个；

因为需要非空的反序列化处理，所以时间传的是2001-1-1；其他的浮点类型的是0.0000，整数类型的是0；

超过了保修期的收费计算：
维修提成额 = (维修费用 - （业务费 * 1.15）) * 10% => 最后的利率是根据单属于光学还是电子的属性尽心区分的，光学的是14.5%，电子的是10%
还在保修期内的收费：
光学/激光：20
电子：15
GPS：15

提成单据生成：
整体的数据对比是恒业、测绘大厦、广分的单汇总进行计算
单号：在xls文件列出；
分项：匹配获取数据单里面的才可以知道；
属性：匹配获取数据单里面的才知道；
属地：匹配获取数据单里面的才知道；
客户名称：匹配获取数据单里面的才知道；
型号：匹配获取数据单里面的才知道；
数量：在xls表里面；
维修金额：匹配获取数据单里面的才知道，价格1；
成本：匹配获取数据单里面的才知道；
业务费：在xls里面；
扣款金额：置为0000；目前是将呢度直接写成0000就可以了
维修提成额：上面就算方法；
完成日期：匹配获取数据单里面的才知道；





